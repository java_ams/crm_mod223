package ch.zli.m223.CRM;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;
import ch.zli.m223.CRM.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTests {

	@Autowired
	CustomerService customerService;
	
	
	@Test
	public void getCustomerListTest() {
		List<Customer> customers = customerService.getCustomerList();
		assertNotNull(customers);
		assertEquals(3, customers.size());
	}
	
	@Test
	public void getCustomerTest() {
		List<Customer> customers = customerService.getCustomerList();
		for(Customer customer : customers) {
			Customer customerRep = customerService.getCustomer(customer.getId());
			assertEquals(customer.getId(), customerRep.getId());
			assertEquals(customer.getName(), customerRep.getName());
			}
	}
	
	@Test
	public void addCustomerTest() {
		List<Customer> customers = customerService.getCustomerList();
		int oldSize = customers.size();
		
		String[] parameter = {"Test", "Teststrasse", "testStadt"};
		customerService.addCustomer(parameter[0], parameter[1], parameter[2]);
		
		customers = customerService.getCustomerList();
		
		assertEquals(customers.size(), oldSize + 1);
		for(Customer customer : customers) {
			if (customer.getName() == parameter[0] &&
				customer.getStreet() == parameter[1] && 
				customer.getCity() == parameter[2])
			customerService.deleteCustomer(customer.getId());
		}
	}
	@Test
	public void addMemoToCustomerTest() {
		List<Customer> customers = customerService.getCustomerList();
		String memotext = "TestStringMemo";
		Memo memo;
		for(Customer customer : customers) {
			long id = customer.getId();
			memo = customerService.addMemoToCustomer(id, memotext);
			assertEquals(memo.getNote(), memotext);	
		}
		memo = customerService.addMemoToCustomer(0, memotext);
		assertNull(memo);
	}
	@Test 
	public void getCoverageDateTest() {
		List<Customer> customers = customerService.getCustomerList();
		for(Customer customer : customers) {
			List<Memo> memos = customer.getMemos();
			for (Memo memo : memos) {
				assertNotNull(memo.getCoverageDate());
			}
		}
	}	

}
