package ch.zli.m223.CRM;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ch.zli.m223.CRM.model.User;
import ch.zli.m223.CRM.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest

public class UserServiceTest {
	
	@Autowired
	UserService userService;
	
	@Test
	public void getAllUsersTest() {
		List<User> users = userService.getAllUsers();
		assertNotNull(users);
		assertEquals(3, users.size());
	}
	@Test
	public void getUserById() {
		List<User> users = userService.getAllUsers();
		for (User user : users) {
			assertNotNull(user);
			User expectedUser = userService.getUserById(user.getId());
			assertEquals(user.getId(), expectedUser.getId());
			assertEquals(user.getUserName(), expectedUser.getUserName());
		}	
	}
	@Test
	public void deleteUserTest() {
		User user;
		String[] parameter = {"TestName", "TestPw", "TestRolle"};
		user = userService.createUser(parameter[0], parameter[1], parameter[2]);
		long id = user.getId();
		userService.deleteUser(id);
		assertNull(userService.getUserById(id));
	}
	@Test 
	public void createUserTest() {
		// Create User and get Id
		User user;
		String[] parameter = {"TestName", "TestPw", "TestRolle"};
		user = userService.createUser(parameter[0], parameter[1], parameter[2]);
		long id = user.getId();
		// Check if user with same name can be created, shouldn't be possible
		user = userService.createUser(parameter[0], parameter[1], parameter[2]);
		assertNull(user);
		// Delete user and check with id if the user was deleted
		userService.deleteUser(id);
		assertNull(userService.getUserById(id));
	}
	@Test
	public void updateRolesTest() {
		List<User> users = userService.getAllUsers();
		String[] expectedRoles = {"Role1", "TestRole", "Rolle"};
		
		for (User user: users) {
			long id = user.getId();
			user = userService.updateRoles(id, expectedRoles);
			Set<String> roles = user.getRoleNames();
			for (String role : expectedRoles) {
				assertTrue(roles.contains(role));
			}
		}
		// Try to update an User that doesn't exit, should return null
		User user = userService.updateRoles(0, expectedRoles);
		assertNull(user);	
	}
	@Test
	public void updatePasswordTest() {
		String[] pws = {"pwAlt", "pwNeu"};
		Boolean answer = userService.updatePassword(0, pws[0], pws[1]);
		assertFalse(answer);
		
		User user = userService.createUser("Tester", pws[0], "tester");
		long id = user.getId();
		
		answer = userService.updatePassword(id, pws[0], pws[1]);
		assertTrue(answer);
		
		answer = userService.updatePassword(id, "xyz", pws[1]);
		assertFalse(answer);
	}


}
