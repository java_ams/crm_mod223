package ch.zli.m223.CRM.security.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import ch.zli.m223.CRM.model.User;
import ch.zli.m223.CRM.security.SpringRole;
import ch.zli.m223.CRM.service.UserService;

@Component
public class UserDetailServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.getUserByName(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("User with name %s not found", username));
		}
		
		
		return new UserDetailsImpl(user);
	}
	
	
	@SuppressWarnings("serial")
	public static class UserDetailsImpl extends org.springframework.security.core.userdetails.User{
		
		private User user;
		
		

		public UserDetailsImpl(User user) {
			super(
					user.getUserName(), 
					user.getPassword(),
					AuthorityUtils.createAuthorityList(
							user.getRoleNames().stream().map(role -> SpringRole.ROLE_PREFIX + role).toArray(String[]::new)
							)
				);
			this.user = user;
		}
		public long getId() { return user.getId(); }
		public Set<String> getRoleNames() { return user.getRoleNames(); } 

		
	}

}
