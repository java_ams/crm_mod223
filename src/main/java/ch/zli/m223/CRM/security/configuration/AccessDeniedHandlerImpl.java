package ch.zli.m223.CRM.security.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler{
	/**
	 * Handle when request ist not allowed
	 */
	@Override
	public void handle(
			HttpServletRequest request, 
			HttpServletResponse response, 
			AccessDeniedException accessDeniedExecption) throws IOException, ServletException {
		
		response.sendRedirect(request.getContextPath() + "/403");
		
		
	}

}
