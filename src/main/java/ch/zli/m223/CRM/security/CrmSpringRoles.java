package ch.zli.m223.CRM.security;

import ch.zli.m223.CRM.role.CrmRoles;

public interface CrmSpringRoles extends SpringRole, CrmRoles{
	public static final String ROLE_USER = ROLE_PREFIX + CrmRoles.USER;
	public static final String ROLE_ADMIN = ROLE_PREFIX + CrmRoles.ADMIN;
	public static final String[] ROLE_ALL = {ROLE_USER, ROLE_ADMIN};
}
