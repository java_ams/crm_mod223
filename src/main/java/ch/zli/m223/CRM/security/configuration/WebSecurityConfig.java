package ch.zli.m223.CRM.security.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import ch.zli.m223.CRM.role.CrmRoles;

@Configuration
//@EnableGlobalMethodSecurity(jsr250Enabled = true) // Add method level security bi using @roles
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private AccessDeniedHandler accessDeniedHandler;
	@Autowired
	private UserDetailsService userDetailsService;
	
	// CrmRoles.ADMIN allow access to /admin/
	// CrmRoles.USER allow access to /user/
	// CrmRoles.All_ROLES allow acces to /authenticatedUsers/
	// 403 (access denis) -> handler
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		
		http
				.authorizeRequests()
				.antMatchers("/", "/home/**", "/public/**" ).permitAll()
				.antMatchers("/admin/**").hasAnyRole(CrmRoles.ADMIN)
				.antMatchers("/user/**").hasAnyRole(CrmRoles.USER)
				.antMatchers("/authenticatedUsers/**").hasAnyRole(CrmRoles.ALL_ROLES)
				.antMatchers("/rest/v1/**").permitAll()

				.anyRequest().authenticated()
			.and()
				.formLogin()
					.loginPage("/login")
					.permitAll()
					.and()
				.logout()
					.permitAll()
					.and()
				.exceptionHandling()
					.accessDeniedHandler(accessDeniedHandler)
			.and()
				.csrf().disable()
			;
					
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {

		web
			.ignoring()
			.antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
		
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth
			.userDetailsService(userDetailsService)
			.passwordEncoder(new BCryptPasswordEncoder());
	}
}
