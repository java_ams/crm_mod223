package ch.zli.m223.CRM.role;

public interface CrmRoles {
	
	public static final String USER = "crm_user";
	public static final String ADMIN = "crm_admin"; 
	public static final String[] ALL_ROLES = {USER, ADMIN}; 

}
