package ch.zli.m223.CRM.model.dto;

import ch.zli.m223.CRM.model.Memo;

public class MemoDto {
	
	private Long Id;
	private Long coverageDate;
	private String note;
	private Long customerId;
	
	/**
	 * Constructor
	 */
	public MemoDto() {
		Id = customerId = null;
		coverageDate = null;
		note = "";
	}
	/**
	 * Constructor
	 * @param memo the actual memo
	 */
	public MemoDto(Memo memo) {
		Id = memo.getId();
		coverageDate = memo.getCoverageDate().getTime();
		note = memo.getNote();
		customerId = memo.getCustomer() == null ? null : memo.getCustomer().getId();
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getCoverageDate() {
		return coverageDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
