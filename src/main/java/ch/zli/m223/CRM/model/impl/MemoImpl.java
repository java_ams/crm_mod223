package ch.zli.m223.CRM.model.impl;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;

@Entity(name="Memo")
public class MemoImpl implements Memo{

	//Fields
	@Id
	@GeneratedValue
	private Long Id;
	private Date coverageDate;
	private String note;

	// Two directional mapping,
	// each memo belongs to exact one customer
	@ManyToOne
	private CustomerImpl customer;
	
	protected MemoImpl() {
		
	}
	/**
	 * 
	 * @param coverageDate
	 * @param note
	 * @param noteId
	 */
	public MemoImpl(Customer customer, String memoText) {
		super();
		this.coverageDate = new Date();
		this.note = memoText;
		this.customer = (CustomerImpl) customer;
	}

	
	@Override
	public Long getId() {
		return Id;
	}

	@Override
	public Date getCoverageDate() {
		return coverageDate;
	}

	@Override
	public String getNote() {
		return note;
	}
	@Override
	public Customer getCustomer() {
		return customer;
	}

	
}
