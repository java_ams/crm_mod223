package ch.zli.m223.CRM.model.impl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ch.zli.m223.CRM.model.User;

@Entity(name="User")
public class UserImpl implements User{
	//Fields
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String passwordHash;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> roles = new HashSet<>();
	
	protected UserImpl() {
		
	}
	public UserImpl(String username, String password, String... roles) {
		this.name = username;
		setPassword(password);
		for (String role : roles) {
			this.roles.add(role);			
		}
	}

	@Override public Long getId() { return id; }

	@Override
	public String getUserName() {
		return name;
	}

//	@Override
//	public String getPassword() {
//		return password;
//	}

	@Override
	public Set<String> getRoleNames() {
		return roles;
	}
	@Override
	public boolean verifyPassword(String password) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(password, passwordHash);
	}
	@Override
	public void updateUserRoles(String... roles) {
		this.roles.clear();
		for (String role : roles) {
			this.roles.add(role);
		}
	}
	@Override
	public void setPassword(String password) {
		BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
		this.passwordHash = bcpe.encode(password);
		
	}
	@Override
	public String getPassword() {
		return passwordHash;
	}
	

}
