package ch.zli.m223.CRM.model.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="Customer")
public class CustomerImpl implements Customer{
	
	
	//Fields
	@Id					//id
	@GeneratedValue 	//auto increment
	private Long id;
	
	private String name;
	private String street;
	private String city;
	
	// Two directional mapping
	// Each customer owns its memos
	// Deleting a customer deletes its memos too.
	@OneToMany(mappedBy="customer", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<MemoImpl> memos;

	
	protected CustomerImpl() {
		
	}
//	private List<Memo> memos;
	/**
	 * Constructor
	 * @param name the name of the costumer
	 * @param street the street of the customer
	 * @param city the city of the customer
	 */
	public CustomerImpl(String name, String street, String city) {
		super();
		this.name = name;
		this.street = street;
		this.city = city;
		this.memos = new ArrayList<>();
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public String getStreet() {
		return street;
	}
	@Override
	public String getCity() {
		return city;
	}
	@Override
	public List<Memo> getMemos() {
		Collections.sort(memos, new Comparator<Memo>() {

			@Override
			public int compare(Memo left, Memo right) {
				// TODO Auto-generated method stub
				return (int)(right.getCoverageDate().getTime() - left.getCoverageDate().getTime());
			}});
		return new ArrayList<>(memos);
	}
	public void addMemo(MemoImpl memo) {
		memos.add(memo);
	}
	public void setName(String name) {
		this.name = name;
	}
	public CustomerImpl update(String name, String street, String city) {
		
		this.name = name;
		this.street = street;
		this.city = city;
		return this;
	}

}
