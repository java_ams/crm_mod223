package ch.zli.m223.CRM.model.dto;

import ch.zli.m223.CRM.model.Customer;

public class CustomerDto {
	// fields
	public Long id;
	private String name;
	private String street;
	private String city;
	/**
	 * Constructor
	 */
	public CustomerDto() {
		id = null;
		name = "";
		street = "";
		city = "";
	}
	/**
	 * Constructor
	 * @param customer the actual customer
	 */
	public CustomerDto(Customer customer) {
		id = customer.getId();
		name = customer.getName();
		street = customer.getStreet();
		city = customer.getCity();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

}
