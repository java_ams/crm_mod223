package ch.zli.m223.CRM.model;

import java.util.Set;

public interface User {
	
	
	/**
	 * 
	 * @return return the user id
	 */
	public Long getId();
	/**
	 * 
	 * @return return the Username
	 */
	public String getUserName();
//	/**
//	 * 
//	 * @return return the password
//	 */
//	public String getPassword();
	/**
	 * 
	 * @return return the returned set of roles
	 */
	public Set<String> getRoleNames();
	/**
	 * Verify a password against the users passowrd
	 * @param password string the passwords as text
	 * @return true if password valid, false otherweise
	 */
	public boolean verifyPassword(String password);
	
	/**
	 * get the saved Passwordhash
	 * @return the saved passwordhash
	 */
	public String getPassword();
	/**
	 * hash and encrypt the raw password
	 * @param password	the raw password
	 */
	public void setPassword(String password);
	/**
	 * update the roles of a user
	 * @param roles the roles of the user
	 */
	public void updateUserRoles(String... roles);
	
}
