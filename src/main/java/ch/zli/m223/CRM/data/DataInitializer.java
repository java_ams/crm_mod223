package ch.zli.m223.CRM.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;
import ch.zli.m223.CRM.role.CrmRoles;
import ch.zli.m223.CRM.security.util.FakeUser;
import ch.zli.m223.CRM.service.CustomerService;
import ch.zli.m223.CRM.service.UserService;

@Component
public class DataInitializer implements ApplicationRunner{

	@Autowired 
	CustomerService customerService;
	@Autowired
	UserService userService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		try {
		makeUsSuper();

		// Create some application data
		Customer c0, c1, c2;
		@SuppressWarnings("unused")
		Memo m0, m1, m2;
		c0 =customerService.addCustomer("Werner Max", "Tobelrainli 6", "5416 Kirchdorf");
		c1 = customerService.addCustomer("Bohli Armin", "Dorfgasse 2", "8853 Lachen");
		c2 = customerService.addCustomer("Ehrensberger Susi", "Heimstrasse 41", "5417 Untersiggenthal");
		customerService.addMemoToCustomer(c0.getId(), "Das ist eine Testmemo");
		customerService.addMemoToCustomer(c1.getId(), "Das ist eine Testmemo");
		customerService.addMemoToCustomer(c1.getId(), "Das ist eine Testmemo");
		customerService.addMemoToCustomer(c2.getId(), "Das ist eine Testmemo");
		customerService.addMemoToCustomer(c0.getId(), "Erstkontakt positiv verlaufen");
		customerService.addMemoToCustomer(c0.getId(), "Sitzung vereinbart");
		customerService.addMemoToCustomer(c0.getId(), "Offerteneingang bestätigt");	
		customerService.addMemoToCustomer(c1.getId(), "Erstkontakt, Kunde unsicher, nachhaken");	
		customerService.addMemoToCustomer(c1.getId(), "Chefsekretärin, Hund heisst Mischu, keine Familie, Ansprechpartner für alles");

		userService.createUser("user", "user", CrmRoles.USER);
		userService.createUser("admin", "admin", CrmRoles.ADMIN);
		userService.createUser("usmin", "usmin", CrmRoles.ALL_ROLES);
		} finally {
		resetSuperUser();
		}
	}
	
	private void resetSuperUser() {
		SecurityContextHolder.clearContext();
	}
	private void makeUsSuper() {
		FakeUser user = new FakeUser(CrmRoles.ALL_ROLES);
		SecurityContextHolder.getContext().setAuthentication(user);
	}


}
