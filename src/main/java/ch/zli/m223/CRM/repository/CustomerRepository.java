package ch.zli.m223.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.impl.CustomerImpl;

public interface CustomerRepository extends JpaRepository<CustomerImpl, Long> {
	
	/**
	 * Create a new customer
	 * @param name	the name of the customer
	 * @param street	the street of the customer
	 * @param city	the city of the customer
	 * @return	the created customer
	 */
	default Customer createCustomer(String name, String street, String city) {
		CustomerImpl customer = new CustomerImpl(name, street, city);
		save(customer);
		return customer;
	}
	/**
	 * Update an existing customer
	 * @param customer	the actual customer
	 * @param name the new name of the customer
	 * @param street the new street of the customer
	 * @param city the new city of the customer
	 * @return	the updated customer
	 */
	default Customer update(Customer customer, String name, String street, String city) {
		return save(((CustomerImpl) customer).update(name, street, city));
	}

}
