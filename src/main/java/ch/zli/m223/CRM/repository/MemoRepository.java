package ch.zli.m223.CRM.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;
import ch.zli.m223.CRM.model.impl.CustomerImpl;
import ch.zli.m223.CRM.model.impl.MemoImpl;

public interface MemoRepository extends JpaRepository<MemoImpl, Long>{
	
	/**
	 * Create a new memo
	 * @param customer the customer which the memo belongs to 
	 * @param memotext	the actual memo
	 * @return	the saved memo
	 */
	default Memo createMemo(Customer customer, String memotext) {
		CustomerImpl customerImpl = (CustomerImpl) customer;
		MemoImpl memo = new MemoImpl(customerImpl, memotext);
		customerImpl.addMemo(memo);
		save(memo);
		return memo;
		
	}

}
