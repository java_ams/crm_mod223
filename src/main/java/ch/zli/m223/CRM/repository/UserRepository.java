package ch.zli.m223.CRM.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import ch.zli.m223.CRM.model.User;
import ch.zli.m223.CRM.model.impl.UserImpl;

public interface UserRepository extends JpaRepository<UserImpl, Long>{
	
	/**
	 * Find user by name
	 * @param name	the name of the user
	 * @return the found user or null if not exists
	 */
	//@Query("select u from User u where u.name = :name")
	User findOneByName(String name);

	/**
	 * Create a new user
	 * @param name	naem of the user
	 * @param password	password of the user
	 * @param roleNames roles of the user
	 * @return the created user
	 */
	default User createUser(String name, String password, String[] roleNames) {
		UserImpl user = new UserImpl(name, password, roleNames);
		
		return save(user);
	}

	/**
	 * update the roles of the user
	 * @param user the user which needs to be updated
	 * @param roleNames	the new roles
	 * @return	the updated user
	 */
	default User updateRolesForUser(User user, String[] roleNames) {
		UserImpl userImpl = (UserImpl)user;
		userImpl.updateUserRoles(roleNames);	
		save(userImpl);
		return userImpl;
	}

	/**
	 * Update the password of the user
	 * @param user	the user which wants the pw to be updated
	 * @param oldPassword	old hashed password
	 * @param newPassword	new raw password
	 * @return	the updated user
	 */
	default boolean updatePasswordForUser(User user, String oldPassword, String newPassword) {
		UserImpl userImpl = (UserImpl) user;
		if (userImpl.verifyPassword(oldPassword)) {
			userImpl.setPassword(newPassword);
			save(userImpl);
			return true;
		}
		return false;
	}


}
