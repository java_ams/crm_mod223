package ch.zli.m223.CRM.service;

import java.util.List;

import ch.zli.m223.CRM.model.User;

public interface UserService {

	/**
	 * get the user by id
	 * @param id the id of the user
	 * @return the user that was found
	 */
	User getUserById(long id);
	/**
	 * find user by name
	 * @param name	the name that needs to be found
	 * @return	the found user
	 */
	User getUserByName(String name);
	/**
	 * get all users
	 * @return	a list of all users
	 */
	List<User> getAllUsers();
	/**
	 * Create a new user
	 * @param name name of the user
	 * @param password	pw of the user
	 * @param roleNames	roles of the user
	 * @return the created user
	 */
	User createUser(String name, String password, String... roleNames);
	/**
	 * delete the user
	 * @param id the id of the user who needs to be deleted
	 */
	void deleteUser(long id);
	/**
	 * upadte the roles of the user
	 * @param id	the id of the user who needs to be updated
	 * @param roleNames	the new roles
	 * @return	the udpated user
	 */
	User updateRoles(long id, String... roleNames);
	/**
	 * updatge pw of user
	 * @param id	id of the user
	 * @param oldPassword	old pw
	 * @param newPassword	new pw
	 * @return	bool 
	 */
	boolean updatePassword(long id, String oldPassword, String newPassword);
}
