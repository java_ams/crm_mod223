package ch.zli.m223.CRM.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.zli.m223.CRM.model.User;
import ch.zli.m223.CRM.repository.UserRepository;
import ch.zli.m223.CRM.role.CrmRoles;
import ch.zli.m223.CRM.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public User getUserById(long id) {	
		return userRepository.findOne(id);
	}

	@Override
	public User getUserByName(String name) {
		return userRepository.findOneByName(name);
	}

	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public List<User> getAllUsers() {
		return new ArrayList<>(userRepository.findAll());
	}

	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public User createUser(String name, String password, String... roleNames) {
		if(getUserByName(name) != null ) {return null;}
		return userRepository.createUser(name, password, roleNames);
	}

	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public void deleteUser(long id) {
		userRepository.delete(id);
		
	}

	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public User updateRoles(long id, String... roleNames) {
		User user = getUserById(id);
		if(user == null) {return null;}
		return userRepository.updateRolesForUser(user, roleNames);
		
	}

	@Override
	@RolesAllowed(CrmRoles.ADMIN)
	public boolean updatePassword(long id, String oldPassword, String newPassword) {
		User user = getUserById(id);
		if (user == null) {return false;}
		return userRepository.updatePasswordForUser(user, oldPassword, newPassword);
	}

}
