package ch.zli.m223.CRM.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.Memo;
import ch.zli.m223.CRM.model.impl.CustomerImpl;
import ch.zli.m223.CRM.repository.CustomerRepository;
import ch.zli.m223.CRM.repository.MemoRepository;
import ch.zli.m223.CRM.role.CrmRoles;
import ch.zli.m223.CRM.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private MemoRepository memoRepository;

	@Override
	@PermitAll
	public List<Customer> getCustomerList() {
		List<Customer> customerList = new ArrayList<Customer>(
				customerRepository.findAll(new Sort("name")));
		return customerList;
	}

	@Override
	@RolesAllowed(CrmRoles.USER)
	public Customer getCustomer(long customerId) {
		CustomerImpl customer = customerRepository.findOne(customerId);
		return customer;
	}

	@Override
	@RolesAllowed(CrmRoles.USER)
	public Customer addCustomer(String name, String street, String city) {	
		return customerRepository.createCustomer(name, street, city);
	}

	@Override
	@RolesAllowed(CrmRoles.USER)
	public Memo addMemoToCustomer(long customerId, String memotext) {	
		Customer customer = getCustomer(customerId);
		if(customer == null) {
			return null;
		}
		return memoRepository.createMemo(customer, memotext);
	}

	@Override
	@RolesAllowed(CrmRoles.USER)
	public void deleteCustomer(long id) {
		customerRepository.delete(id);	
	}

	@Override
	@RolesAllowed(CrmRoles.USER)
	public Customer updateCustomer(Long id, String name, String street, String city) {
		
		Customer customer = customerRepository.getOne(id);
		if (customer == null) {
			return null;			
		}
		return customerRepository.update(customer, name, street, city);
	}
	
	
}
