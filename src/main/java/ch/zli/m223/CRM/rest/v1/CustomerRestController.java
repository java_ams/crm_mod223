package ch.zli.m223.CRM.rest.v1;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.model.dto.CustomerDto;
import ch.zli.m223.CRM.service.CustomerService;

@RestController
public class CustomerRestController {
	
	@Autowired
	private CustomerService customerService;
	
	/**
	 * Get all customers
	 * @return a (possibly empty) list of customers
	 */
	@RequestMapping(value="/rest/v1/customers", method=RequestMethod.GET)
	public List<CustomerDto> getCustomerList(){

		List<Customer> customers = customerService.getCustomerList();
		 
	    return customers.stream().map(cust -> new CustomerDto(cust)).collect(Collectors.toList());
		
		
	}
	
	//CRUD
	/**
	 * Create a new customer
	 * @param customer	the new customer
	 * @return the created customer
	 */
	@RequestMapping(value="/rest/v1/customer/create", method=RequestMethod.POST)
	public CustomerDto createCustomer(@RequestBody CustomerDto customer) {
		Customer cus = customerService.addCustomer(customer.getName(), customer.getStreet(), customer.getCity());
		CustomerDto custDto = new CustomerDto(cus);
		return custDto;
		
	}
	/**
	 * Create a new customer
	 * @param name	the customers name
	 * @param street	the customeres street
	 * @param city	the customers city
	 * @return the created customer
	 */
	
	/**
	 * 
	 * @param id the customers id
	 * @return a customer or null if this customer doesn't exist
	 */
	@RequestMapping(value="/rest/v1/customer/{id}", method=RequestMethod.GET)
	public CustomerDto readCustomer(@PathVariable("id") long id) {
		Customer customer = customerService.getCustomer(id);
		CustomerDto custDto = new CustomerDto(customer);
		return custDto;
	}
	/**
	 * 
	 * @param customer the customer to update
	 * @return the updated customer
	 */
	@RequestMapping(value="/rest/v1/customer/update/{id}", method=RequestMethod.POST)
	public CustomerDto updateCustomer(@RequestBody CustomerDto customer) {
		Customer cus = customerService.updateCustomer(customer.getId(), customer.getName(), customer.getStreet(), customer.getCity());
		CustomerDto cusDto = new CustomerDto(cus);
		return cusDto;
	}
	/**
	 * 
	 * @param customer the customer to delete
	 */
	@RequestMapping(value="/rest/v1/customer/delete/{id}", method=RequestMethod.POST)
	public void deleteCustomer(@RequestBody CustomerDto customer) {
		customerService.deleteCustomer(customer.getId());
	}

}
