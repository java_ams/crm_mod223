package ch.zli.m223.CRM.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.zli.m223.CRM.model.Customer;
import ch.zli.m223.CRM.service.CustomerService;

@Controller
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping("/")
	String root() {
		return "redirect:/home";
	}
	
	@RequestMapping("/home")
	public String showCustomerList(Model model) {
		List<Customer> customers = customerService.getCustomerList();
		model.addAttribute("customers", customers);
		return "showCustomerList";
	}
	
	@RequestMapping("/user/showCustomer/{id}")
	public String showCustomer(Model model, @PathVariable("id") long customerId){
		// get the customer object from customerService
		// store it as "customer" in the model
		// and return "showCustomer" as the new view
		Customer customer = customerService.getCustomer(customerId);
		model.addAttribute("customer", customer);
		return "showCustomer";
	}
	
	@RequestMapping("/user/addMemo")
	public String addMemo(Model model, @RequestParam("customerId") long customerId, @RequestParam("memoText") String memoText ) {
		customerService.addMemoToCustomer(customerId, memoText);
		return "redirect:/user/showCustomer/" + customerId;
	}


}
